## Novelus Book Store

** Thank you for showing interest in working with us. Below you will find an assignment which will test some of the practical skills we require.**

Below you will find the details needed to finish the assignment. 

---

### Required skills to finish the assignment

- Laravel 5 (5.5, 5.6, 5,7. 5.8)
- PHPUnit 7.X
- VueJS 2.X, Axios
- Bootstrap 4.X or TAILWINDCSS 1.X
- REST/JSON/API/HTTP
- TDD
- GIT

---

## Prepare your project

Since this is Laravel and Vue assignment, you first have to create a Laravel project. The Vue is already set up for you
and you may continue working on - create components and use them inside .blade.php files.

For extra points, you can create Vue SPA application.

---

## Assignment

You will be creating a simple Book Store application which will allow it's users to find and rent books but also help admins to have
complete control over books they have currently in store. 

### Functionalities

#### API (Backend)

1. Retrieve a list of all books in the store
2. Make a reservation for a book - user can make a reservation for a book for a specific period if that book is available (quantity), admin can reserve a book for the user - reserving a book does not mean renting a book
3. Rent a book - admin can rent a book to the user if the book is available for a specific period
4. Admin can complete the reservation when the book is returned
5. User can choose a rent time - 10 days, 20 days, 30 days
6. Admin can see all current rents and reservations
7. Admin can see, who has a specific book
8. Admin can add new books, can edit existing books, can change quantitiy of specific books, can delete books
9. Admin can search for books - by title, author, year

Every request has to be authenticated unless there is a specific reason - e.g. a list of books can be retrieved publicly.

All functionalities on backend should be tested, preferably the whole app should be developed in a test driven manner.

Don't forget: validation, authorization, authentication. Use best practices for Laravel.

#### Frontend

All the backend functionalities should be consumed by Vue components. We should be able to log in as a user or as an admin and have all the functionalities for us to use. Frontend does not have to be unit tested.

If you use components inside blade templates, make requests for data from components!

Don't forget: validation, authorization, authentication. Use best practices for Vue.

#### Database

All the tables and relations should be designed based on API functionalities above. Since the assignment can be done in different
ways, we let you decide the best way you can design the database.

#### Source Control

Create a free GitHub or Bitbucket repository and push your code to a remote repository. It is important to note that we will also check
how you create your commits (message and amount of code per each commit) so take this as an important part of an assignment. We will
not check the assignments with only 'initial commit'.

Your README should contain information about how can we set up your project on our machine.

---

## How about the server? 

You can do it however you want it: WAMP, MAMP, XAMP, Vagrant, Valet, Docker .... It is up to you to decide - when we will check your 
product, we will deploy it to Homestead (Vagrant Box) and our internal development server - unless you choose Docker. 

---

## Help

- https://www.youtube.com/watch?v=MF0jFKvS4SI&t=1488s
- https://laravel.com/docs/5.8
- https://vuejs.org/v2/guide/